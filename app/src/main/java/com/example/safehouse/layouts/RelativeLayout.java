package com.example.safehouse.layouts;

import android.app.Activity;
import android.graphics.Color;
import android.media.Image;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class RelativeLayout extends Activity {

    ImageView RedBox, YellowBox, BlueBox, PurpleBox, GreenBox;
    Button btnRed, btnBlue, btnReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative_layout);

        btnRed = (Button) findViewById(R.id.btnRed);
        btnBlue = (Button) findViewById(R.id.btnBlue);
        btnReset = (Button) findViewById(R.id.btnReset);
        RedBox = (ImageView) findViewById(R.id.RedBox);
        YellowBox = (ImageView) findViewById(R.id.YellowBox);
        BlueBox = (ImageView) findViewById(R.id.BlueBox);
        PurpleBox = (ImageView) findViewById(R.id.PurpleBox);
        GreenBox = (ImageView) findViewById(R.id.GreenBox);

        btnRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RedBox.setBackgroundColor(Color.parseColor("#ff0000"));
                YellowBox.setBackgroundColor(Color.parseColor("#ff0000"));
                BlueBox.setBackgroundColor(Color.parseColor("#ff0000"));
                PurpleBox.setBackgroundColor(Color.parseColor("#ff0000"));
                GreenBox.setBackgroundColor(Color.parseColor("#ff0000"));
            }
        });

        btnBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RedBox.setBackgroundColor(Color.parseColor("#001eff"));
                YellowBox.setBackgroundColor(Color.parseColor("#001eff"));
                BlueBox.setBackgroundColor(Color.parseColor("#001eff"));
                PurpleBox.setBackgroundColor(Color.parseColor("#001eff"));
                GreenBox.setBackgroundColor(Color.parseColor("#001eff"));
            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RedBox.setBackgroundColor(Color.parseColor("#ff0000"));
                YellowBox.setBackgroundColor(Color.parseColor("#ffff00"));
                BlueBox.setBackgroundColor(Color.parseColor("#001eff"));
                PurpleBox.setBackgroundColor(Color.parseColor("#6f00ff"));
                GreenBox.setBackgroundColor(Color.parseColor("#1aff00"));
            }
        });
    }



}
